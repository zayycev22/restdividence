from django.urls import path
from app.views import *

urlpatterns = [
    path('users/', user_list),
    path('companies/', CompanyList.as_view()),
    path('me/', me),
    path('login/', try_login),
    path('reg/', try_reg),
    path('user_companies/', user_companies),
    path('available_companies/', available_companies),
    path('update_companies/', add_companies),
    path('update_company/', update_company),
    path('remove_company/', remove_company),
    path('check_date/', check_date),
]