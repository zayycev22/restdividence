from datetime import datetime

from django.contrib.auth import authenticate
from django.core.handlers.wsgi import WSGIRequest
from rest_framework import permissions
from rest_framework.decorators import api_view, permission_classes
from rest_framework.generics import ListAPIView
from rest_framework.renderers import JSONRenderer
from rest_framework.response import Response
from rest_framework.authtoken.models import Token
from app.funcs import username_exists
from app.serializers import *
from rest_framework.request import Request as RestRequest
from app.models import ExUser
from django.shortcuts import get_object_or_404


# Create your views here.
@api_view(http_method_names=['GET'])
@permission_classes((permissions.IsAdminUser,))
def user_list(request: RestRequest):
    """
    The user_list function is a view that returns a list of all users.
    
    :param request: RestRequest: Get the request object
    :return: A list of users
    :doc-author: zayycev22
    """
    queryset = ExUser.objects.all()
    data = UserSerializer(queryset, many=True)
    return Response(data.data)


class CompanyList(ListAPIView):
    queryset = Company.objects.all()
    serializer_class = CompanySerializer
    permission_classes = [permissions.AllowAny, ]


@api_view(http_method_names=['GET'])
def me(request: RestRequest):
    """
    The me function is a view that returns the current user's information.
    
    :param request: RestRequest: Pass the request object to the me function
    :return: A serialized version of the user object
    :doc-author: zayycev22
    """
    user = ExUser.objects.prefetch_related('usercompany_set', 'usercompany_set__company').get(id=request.user.id)
    data = UserSerializer(user)
    return Response(data.data)


@api_view(http_method_names=['POST'])
@permission_classes([permissions.AllowAny])
def try_reg(request: RestRequest):
    """
    The try_reg function is used to register a new user.
    It takes in the username and password of the user, and checks if that username already exists.
    If it does not exist, then it creates a new ExUser object with that username and password, 
    and returns an HTTP 200 response containing the status &quot;registered&quot; as well as their token key. 
    Otherwise, if the username already exists in our database (i.e., there is another ExUser object with that same name), 
    then we return an HTTP 400 response containing only the status &quot;username exists&quot;. 
    
    :param request: RestRequest: Get the data from the request
    :return: A response object
    :doc-author: zayycev22
    """
    username = request.data.get('username')
    password = request.data.get('password')
    if not username_exists(username):
        user = ExUser.objects.create(username=username, password=password)
        token = Token.objects.get(user=user)
        return Response({'status': "registered", 'token': token.key}, status=200)
    else:
        return Response({'status': "username exists"}, status=400)


@api_view(http_method_names=['POST'])
@permission_classes([permissions.AllowAny])
def try_login(request: RestRequest):
    """
    The try_login function takes a request object and attempts to authenticate the user.
    If successful, it returns a token that can be used for future requests.
    
    
    :param request: RestRequest: Get the data from the request
    :return: A response with a status code and some data
    :doc-author: zayycev22
    """
    username = request.data.get('username')
    password = request.data.get('password')
    user = authenticate(username=username, password=password)
    if user is not None:
        token = Token.objects.get(user=user)
        return Response({'status': "logged in", 'token': token.key}, status=200)
    else:
        return Response({'status': 'no such user'}, status=400)


@api_view(http_method_names=['GET'])
def user_companies(request: RestRequest):
    """
    The user_companies function returns a list of companies that the user is associated with.
    
    :param request: RestRequest: Get the user id from the request
    :return: A list of the companies that a user is associated with
    :doc-author: zayycev22
    """
    companies = UserCompany.objects.select_related('company').filter(user=request.user)
    data = UserCompanySerializer(companies, many=True)
    return Response(data.data)


@api_view(http_method_names=['GET'])
def available_companies(request: RestRequest):
    """
    The available_companies function returns a list of companies that the user is not already associated with.
    
    :param request: RestRequest: Get the user object from the request
    :return: A list of company objects that are not in the user's companies
    :doc-author: zayycev22
    """
    companies = Company.objects.exclude(id__in=list(request.user.companies.values_list('id', flat=True)))
    return Response(CompanySerializer(companies, many=True).data)


@api_view(http_method_names=['PUT'])
def add_companies(request: RestRequest):
    """
    The add_companies function takes in a list of companies and adds them to the user's portfolio.
    The function first checks if all the companies exist, then it adds them to the user's portfolio.
    If any of the companies do not exist, an error is returned.
    
    :param request: RestRequest: Get the user object from the request
    :return: A response object with status code 200 if all companies are added to the user's
    :doc-author: zayycev22
    """
    companies: list = request.data
    companies_names = [company['name'] for company in companies]
    companies_obj = Company.objects.filter(name__in=companies_names).values_list('id', flat=True)
    if len(companies_obj) == len(companies):
        request.user.companies.add(*companies_obj, through_defaults={'equity_amount': 1})
        return Response({'status': "Companies added"}, status=200)
    return Response({'status': "No such companies"}, status=400)


@api_view(http_method_names=['PUT'])
def update_company(request: RestRequest):
    """
    The update_company function is used to update the equity amount of a company for a user.
        The function takes in the request object, which contains information about the user and
        company that needs to be updated. It then gets the ExUser object from Django's User model,
        using its id as an identifier. Next it gets the Company object from Django's Company model,
        using its name as an identifier. Then it uses both objects to get a UserCompany instance from
        Django's UserCompany model (which is essentially just a join table between users and companies).

    :param request: RestRequest: Get the data from the request
    :return: The following response:
    :doc-author: zayycev22
    """
    user = ExUser.objects.get(id=request.user.pk)
    comp = get_object_or_404(Company, name=request.data['name'])
    user_comp = UserCompany.objects.select_related('company').get(user=user, company=comp)
    user_comp.equity_amount = request.data['equity_amount']
    user_comp.save()
    return Response({'status': 'user company updated'}, status=200)


@api_view(http_method_names=['PUT'])
def remove_company(request: RestRequest):
    """
    The remove_company function is used to remove a company from the user's list of companies.
        The function takes in a request object, which contains the name of the company that needs to be removed.
        The function then finds and deletes this UserCompany object.

    :param request: RestRequest: Get the user object from the request
    :return: A response with a status of 200
    :doc-author: zayycev22
    """
    user = ExUser.objects.get(id=request.user.pk)
    comp = get_object_or_404(Company, name=request.data['name'])
    UserCompany.objects.get(user=user, company=comp).delete()
    return Response({'status': 'user company removed'}, status=200)


@api_view(http_method_names=['POST'])
def check_date(request: RestRequest):
    day = int(request.data['day'])
    month = int(request.data['month'])
    if month == datetime.now().month:
        cc = Company.objects.filter(day_of_moth=day)
        companies = UserCompany.objects.select_related('company').filter(user=request.user, company__in=cc)
        print(str(companies.query))
        data = UserCompanySerializer(companies, many=True)
        if len(data.data) == 0:
            return Response({"status": "no data"}, status=400)
        return Response(data.data, status=200)
    else:
        return Response({"status": "no data"}, status=400)
# {"month": 5, "day": 0}
