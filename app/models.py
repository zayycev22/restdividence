from django.contrib.auth.models import AbstractUser
from django.db import models


class Company(models.Model):
    name = models.CharField(max_length=50, verbose_name="Название компании",  unique=True)
    equity_price = models.FloatField(verbose_name="Цена за акцию")
    company_div = models.FloatField(verbose_name="Процент дивидендов")
    day_of_moth = models.IntegerField(default=0)

    def __str__(self):
        return self.name

    class Meta:
        verbose_name = "Компания"
        verbose_name_plural = "Компании"


class ExUser(AbstractUser):
    companies = models.ManyToManyField(Company, through='UserCompany')
    REQUIRED_FIELDS = []

    def __str__(self):
        return self.username

    class Meta:
        verbose_name = "Пользователь"
        verbose_name_plural = "Пользователи"


class UserCompany(models.Model):
    user = models.ForeignKey(ExUser, on_delete=models.CASCADE)
    company = models.ForeignKey(Company, on_delete=models.CASCADE)
    equity_amount = models.IntegerField(default=1)

    class Meta:
        verbose_name = "Компания пользователя"
        verbose_name_plural = "Пользователи"
        unique_together = ('user', 'company')
