from rest_framework import serializers
from .models import ExUser, Company, UserCompany


class CompanySerializer(serializers.ModelSerializer):
    class Meta:
        model = Company
        fields = ("name", "equity_price", "company_div", "day_of_moth")


class UserCompanySerializer(serializers.ModelSerializer):
    name = serializers.CharField(source='company.name')
    equity_price = serializers.DecimalField(source='company.equity_price', max_digits=10, decimal_places=2)
    company_div = serializers.DecimalField(source='company.company_div', max_digits=10, decimal_places=2)
    day_of_month = serializers.IntegerField(source='company.day_of_moth')

    class Meta:
        model = UserCompany
        fields = ('name', 'equity_price', 'company_div', 'equity_amount', "day_of_month")


class UserSerializer(serializers.ModelSerializer):
    user_companies = UserCompanySerializer(source='usercompany_set', many=True)

    class Meta:
        model = ExUser
        fields = ('username', 'user_companies')
