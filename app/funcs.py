from app.models import ExUser


def username_exists(username):
    return ExUser.objects.filter(username=username).exists()
