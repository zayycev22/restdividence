from django.contrib import admin

from app.models import ExUser, Company, UserCompany


class UserCompanyInline(admin.StackedInline):
    model = UserCompany
    extra = 1


class ExUserAdmin(admin.ModelAdmin):
    inlines = [UserCompanyInline]


# Register your models here.
admin.site.register(ExUser, ExUserAdmin)
admin.site.register(Company)
